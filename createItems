import groovy.json.JsonSlurperClassic
import groovy.json.JsonOutput
import groovy.json.StringEscapeUtils

String jsonDump(def data) {
    def json = new groovy.json.JsonBuilder(data).toString()
    json = StringEscapeUtils.unescapeJavaScript(json)
    return json
}

//import utils.JJBGenerator

// Инициализируем базовые настройки
def root_dir_path = ROOT_DIR_FROM_PIPELINE
String root_conf_dir = "${root_dir_path}/config"
String root_conf_name = "config.json"
def base_config = new groovy.json.JsonSlurperClassic().parseText(readFileFromWorkspace("${root_conf_dir}/${root_conf_name}"))

def deploy_pipeline_script = readFileFromWorkspace("${root_dir_path}/pipelines/deploy.Jenkinsfile")
def job_runner_pipeline_script = readFileFromWorkspace("${root_dir_path}/pipelines/job_runner.Jenkinsfile")
def restart_pipeline_script = readFileFromWorkspace("${root_dir_path}/pipelines/restart.Jenkinsfile")

def gitHttpUrl = base_config.global_parameters.find{ it.name == 'GIT_HTTP_URL'}.value

// Подготавливаем корневую директорию
folder(base_config.root_dir) {
    authorization {
    }
    description("Операции по установке/конфигурированию/ops сценариям продуктов.")
    properties { 
    folderLibraries { 
      libraries {
          base_config.sharedLibraries.each{ sl ->
              libraryConfiguration {
                name(sl.name)
                retriever {
                        modernSCM {
                            scm {
                                git {
                                    id sl.id
                                    remote sl.remote
                                }
                            }
                        }
                    }
                allowVersionOverride true
                defaultVersion 'master'
                implicit true
                }
          }
      }
    }
  }
}

// Подготавливаем каталоги для джобов
['DEPLOY','RESTART','WORKFLOW'].each{ f ->
    folder("${base_config.root_dir}/${f}") {
        authorization {
        }
    }

    if (f == 'WORKFLOW'){
        def workflow_branch_param_script = readFileFromWorkspace("${root_dir_path}/templates/parameters/workflower/BRANCH_template.groovy").stripIndent()

        base_config.jobs.each{ job_name, job_content ->
            pipelineJob("${base_config.root_dir}/${f}/WORKFLOW_${job_name}") {
                parameters {
                    choiceParam('ZONE', base_config.zones.collect{it.key}, 'Зона для деплоя')

                    activeChoiceReactiveParam('BRANCH') {
                        choiceType('SINGLE_SELECT')
                        groovyScript {
                            script("""
                            def INVENTORY_PROJECT = '${gitHttpUrl}${job_content.parameters.INVENTORY_PROJECT.value}'
                            """.stripIndent()+workflow_branch_param_script)
                            fallbackScript('')
                        }
                        referencedParameter('ZONE')
                    }
                    stringParam ('TICKET_NUMBER', '', 'Номер тикета')
                    nonStoredPasswordParam('PASSWORD', 'Пароль текущего пользователя')
                }

                definition {
                    cps {
                        script("""
                        @Library('sharedlibraryexample@master') _
                        import groovy.json.JsonSlurperClassic;
                        //serrialazable parsing json
                        @NonCPS
                        def jsonParse(def json) {
                            new groovy.json.JsonSlurperClassic().parseText(json)
                        }

                        workflow(
                            [
                                inventory_repo: "${gitHttpUrl}${job_content.parameters.INVENTORY_PROJECT.value}",
                                ansibleRolesProject: "${gitHttpUrl}${job_content.parameters.INSTALLER_REPO.value}",
                                playbook: "${job_content.parameters.PLAYBOOK.value}",
                                branch: "master", 
                                zones: jsonParse('${jsonDump(base_config.zones)}') ,
                                password: PASSWORD,
                                runZone: ZONE,
                                jobsRootDir: '${base_config.root_dir}/DEPLOY',
                                ticketNumber: TICKET_NUMBER
                            ]
                        )
                        """.stripIndent())
                        //sandbox()
                    }
                }

                properties {
                    buildDiscarder {
                        strategy {
                            logRotator {
                                daysToKeepStr("60")
                                numToKeepStr("20")
                                artifactDaysToKeepStr("")
                                artifactNumToKeepStr("")
                            }
                        }
                    }
                }
            }
        }
    }

    if (['DEPLOY','RESTART'].contains(f)){
        
        def preload_param_script = readFileFromWorkspace("${root_dir_path}/templates/parameters/deployer/PRELOAD.groovy").stripIndent()
        def jobs_template_param_script = readFileFromWorkspace("${root_dir_path}/templates/parameters/deployer/JOBS_template.groovy").stripIndent()
        def branch_param_script = readFileFromWorkspace("${root_dir_path}/templates/parameters/deployer/BRANCH.groovy").stripIndent()
        def limits_param_script = readFileFromWorkspace("${root_dir_path}/templates/parameters/deployer/LIMITS.groovy").stripIndent()
        def secret_param_script = readFileFromWorkspace("${root_dir_path}/templates/parameters/deployer/SECRET_EXTRA_VARS.groovy").stripIndent()
        def tags_param_script = readFileFromWorkspace("${root_dir_path}/templates/parameters/deployer/TAGS.groovy").stripIndent()
        def verbose_param_script = readFileFromWorkspace("${root_dir_path}/templates/parameters/deployer/VERBOSE.groovy").stripIndent()
        def user_param_script = readFileFromWorkspace("${root_dir_path}/templates/parameters/deployer/USERNAME").stripIndent()
        
        def restarter_param_script = readFileFromWorkspace("${root_dir_path}/templates/parameters/restarter/RUN_DATA_template.groovy").stripIndent()
        

        // Подготовка папок по зонам
        base_config.zones.each{ zone, content ->
            folder("${base_config.root_dir}/${f}/${content.label}") {
                authorization {
                }
                description("Операции по окружению ${zone}")
            }

            if(f == 'RESTART'){
                base_config.jobs.each{ job_name, job_content ->

                    def restarter_param_value = """
                        def repo_url = "${gitHttpUrl}${job_content.parameters.INVENTORY_PROJECT.value}"
                        def zone = "${zone}"
                        def playbook_path = "${job_content.parameters.PLAYBOOK.value}"
                        def jobName = "${job_name}"
                        def jobFullName = "${base_config.root_dir}/DEPLOY/${content.label}/PRODUCTS/${job_name}"
                    """.stripIndent()

                    pipelineJob("${base_config.root_dir}/${f}/${content.label}/RESTART_${job_name}") {
                        parameters {
                            activeChoiceReactiveReferenceParam('RUN_DATA') {
                                omitValueField()
                                choiceType('FORMATTED_HTML')
                                groovyScript {
                                    script( restarter_param_value + restarter_param_script)
                                    fallbackScript('')
                                }
                                referencedParameter('')
                            }

                            nonStoredPasswordParam('PASSWORD', 'Пароль текущего пользователя')
                        }

                        definition {
                            cps {
                                script(restart_pipeline_script)
                                //sandbox()
                            }
                        }

                        properties {
                            buildDiscarder {
                                strategy {
                                    logRotator {
                                        daysToKeepStr("60")
                                        numToKeepStr("20")
                                        artifactDaysToKeepStr("")
                                        artifactNumToKeepStr("")
                                    }
                                }
                            }
                        }


                    }
                }

            }


            //Генерация джобов для деплоя
            if(f == 'DEPLOY'){
                folder("${base_config.root_dir}/${f}/${content.label}/PRODUCTS") {
                    authorization {
                    }
                }
                def jobs_param_vars = """
                def jobsPath = '${base_config.root_dir}/${f}/${content.label}'
                
                """.stripIndent()

                // Генерируем Job_runner
                pipelineJob("${base_config.root_dir}/${f}/${content.label}/JOB_RUNNER") {
                    parameters {
                        activeChoiceReactiveReferenceParam('PRELOAD') {
                            omitValueField()
                            choiceType('FORMATTED_HIDDEN_HTML')
                            groovyScript {
                                script(preload_param_script)
                                fallbackScript('')
                            }
                            referencedParameter('')
                        }

                        wReadonlyStringParameterDefinition {
                            name("ZONE")
                            defaultValue(zone)
                            description('Текущая зона деплоя')
                        }

                        activeChoiceReactiveReferenceParam('JOBS') {
                            omitValueField()
                            choiceType('FORMATTED_HTML')
                            groovyScript {
                                script(jobs_param_vars+jobs_template_param_script)
                                fallbackScript('')
                            }
                            referencedParameter('')
                        }

                        activeChoiceReactiveParam('BRANCH') {
                            choiceType('SINGLE_SELECT')
                            groovyScript {
                                script(branch_param_script)
                                fallbackScript('')
                            }
                            referencedParameter('JOBS')
                        }

                        activeChoiceReactiveParam('LIMITS') {
                            choiceType('MULTI_SELECT')
                            groovyScript {
                                script(limits_param_script)
                                fallbackScript('')
                            }
                            referencedParameter('ZONE')
                            referencedParameter('JOBS')
                            referencedParameter('BRANCH')
                        }

                        activeChoiceReactiveParam('TAGS') {
                            choiceType('MULTI_SELECT')
                            groovyScript {
                                script(tags_param_script)
                                fallbackScript('')
                            }
                            referencedParameter('JOBS')
                            referencedParameter('BRANCH')
                        }

                        choiceParam('FORKS', ['5', '1', '10'], '')

                        activeChoiceReactiveParam('VERBOSE') {
                            choiceType('SINGLE_SELECT')
                            groovyScript {
                                script(verbose_param_script)
                                fallbackScript('')
                            }
                            referencedParameter('')
                        }

                        activeChoiceReactiveReferenceParam('SECRET_EXTRA_VARS') {
                            omitValueField()
                            choiceType('FORMATTED_HTML')
                            groovyScript {
                                script(secret_param_script)
                                fallbackScript('')
                            }
                            referencedParameter('')
                        }

                        activeChoiceReactiveReferenceParam('USERNAME') {
                            omitValueField()
                            choiceType('FORMATTED_HIDDEN_HTML')
                            groovyScript {
                                script(user_param_script)
                                fallbackScript('')
                            }
                            referencedParameter('')
                        }
                        stringParam ('AGENT_LABEL', 'master', '')
                        
                        nonStoredPasswordParam('PASSWORD', 'Пароль текущего пользователя')

                    }

                    definition {
                            cps {
                                script(job_runner_pipeline_script)
                                //sandbox()
                            }
                    }

                    properties {
                        buildDiscarder {
                            strategy {
                                logRotator {
                                    daysToKeepStr("60")
                                    numToKeepStr("20")
                                    artifactDaysToKeepStr("")
                                    artifactNumToKeepStr("")
                                }
                            }
                        }
                    }

                }


                
                 base_config.jobs.each{ job_name, job_content ->
                    pipelineJob("${base_config.root_dir}/${f}/${content.label}/PRODUCTS/${job_name}") {
                        parameters {
                            
                            base_config.global_parameters.each{ g_param ->
                                wReadonlyStringParameterDefinition {
                                    name(g_param.name)
                                    defaultValue(g_param.value)
                                    description(g_param.description)
                                }
                            }

                            stringParam ('TICKET_NUMBER', '', 'Номер тикета')
                            stringParam ('AGENT_LABEL', 'master', 'Агент Jenkins')

                            wReadonlyStringParameterDefinition {
                                name('ZONE')
                                defaultValue(zone)
                                description('Текущая зона деплоя')
                            }


                            wReadonlyStringParameterDefinition {
                                name('INVENTORY_PROJECT')
                                defaultValue(job_content.parameters.INVENTORY_PROJECT.value)
                                description('Проект с inventory репозиторием')
                            }
                            

                            stringParam ('BRANCH', 'master', 'Ветка репозитория с inventory')
                            stringParam ('USE_TAGS', 'false', 'Будут ли использоваться тэги вместо веток')

                            wReadonlyStringParameterDefinition {
                                name('PLAYBOOK')
                                defaultValue(job_content.parameters.PLAYBOOK.value)
                                description('Плэйбук для запуска')
                            }

                            wReadonlyStringParameterDefinition {
                                name('AUTODEPLOY_SH')
                                defaultValue((job_content.parameters.PLAYBOOK.value.endsWith('.sh'))?'true':'false')
                                description('Используется ли auto-deploy.sh')
                            }

                            stringParam ('VERBOSE', 'v', 'Уровень логирования')
                            stringParam ('FORKS', '5', '')
                            stringParam ('LIMITS', '', 'Лимиты инсталлятора')
                            stringParam ('TAGS', 'all', 'Основные операции над приложением')
                            stringParam ('EXTRA_OPTIONS', '', 'Дополнительные режимы ansible')

                            nonStoredPasswordParam('PASSWORD', 'Пароль текущего пользователя')
                            nonStoredPasswordParam('SECRET_EXTRA_VARS', 'Секретные переменные для инсталлятора')
                            }


                        definition {
                            cps {
                                script(deploy_pipeline_script)
                                //sandbox()
                            }
                        }

                        properties {
                            buildDiscarder {
                                strategy {
                                    logRotator {
                                        daysToKeepStr("60")
                                        numToKeepStr("20")
                                        artifactDaysToKeepStr("")
                                        artifactNumToKeepStr("")
                                    }
                                }
                            }
                        }
                    }
                 }

            }
            
        }
        
    
    }
}


// Создаем основные джобы и папки






