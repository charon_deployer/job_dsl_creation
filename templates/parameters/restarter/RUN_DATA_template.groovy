
def inventoryWorkDir = repo_url.replaceAll(/(https|http):\/+.*?\//,'').replace('.git','')

import org.eclipse.jgit.api.Git;
import groovy.json.JsonOutput
import groovy.text.GStringTemplateEngine
import org.jenkinsci.plugins.pipeline.utility.steps.shaded.org.yaml.snakeyaml.Yaml

def templateText(String text, def binding = [:]){
        //def engine = new groovy.text.SimpleTemplateEngine()
        String res = new groovy.text.GStringTemplateEngine().createTemplate(text).make(binding).toString()
        return res
}

def jsonDump(def data) {
        //but if writeJSON not supported by your version:
        //convert maps/arrays to json formatted string
        def json = JsonOutput.toJson(data)
        //if you need pretty print (multiline) json
        json = JsonOutput.prettyPrint(json)
        return json
}

def jsonParse(def json) {
        new groovy.json.JsonSlurperClassic().parseText(json)
}

def parseYaml(filepath){
    Yaml parser = new Yaml()
    def res = parser.load((filepath as File).text)
    return res
}

def cloneGitRepo(String path, String repo_url, String ref, String refType){
    File workDir = new File('/tmp/caches/', "${path}/${ref}");
    def cloneRef = ref
    if (refType && refType == "tags"){
        cloneRef = "master"
    }
    boolean exists = workDir.exists();
    if (! exists){
        Git.cloneRepository().setURI(repo_url).setDirectory(workDir)
        .setBare(false)
        .setBranch( "refs/heads/${cloneRef}" )
        .setNoCheckout(false)
        .call().close();
    }

    Git git = Git.open(workDir);
    if (refType && refType == "tags"){
        git.checkout().setName("master").call();
        git.pull().call();
        git.checkout().setName(ref).call();
    }else{
        git.pull().call();
    }
    git.close();
    return workDir
}

class InventoryParser implements Serializable{
    private final String inventoryPath
    private inventoryStruct = [:]

    InventoryParser(inventoryPath) {
        this.inventoryPath = inventoryPath
        this.inventoryStruct = getInventoryContent()
        //this.inventoryStruct = filterInventory()
    }

    def initWithMap(def inventory){
        this.inventoryStruct = inventory
    }

    def jsonParse(def json) {
        new groovy.json.JsonSlurperClassic().parseText(json)
    }

    def getInventoryContent(){
        def sout = new StringBuilder(), serr = new StringBuilder()

        String command = "ansible-inventory -i ${this.inventoryPath} --list -vvv"
        println command
        def proc = command.execute()
        proc.consumeProcessOutput(sout, serr)
        proc.waitForOrKill(10000)
        println(sout)
        println(serr)
        def infoWarning = sout.substring(0, sout.indexOf("\n{"))
        def inventoryString =  sout.substring(sout.indexOf("\n{"))

        def inventory = jsonParse(inventoryString)
        inventory.remove('_meta')
        return inventory
    }

    def getInventoryHosts(String limits){
        def sout = new StringBuilder(), serr = new StringBuilder()

        String command = "ansible all -i ${this.inventoryPath} -l ${limits} --list-hosts"
        println command
        def proc = command.execute()
        proc.consumeProcessOutput(sout, serr)
        proc.waitForOrKill(10000)
        println(sout)
        println(serr)
        def info = sout.substring(0, sout.indexOf("):\n"))
        def hostsString = sout.substring(sout.indexOf("):\n"))
        println(hostsString)

        def res = hostsString.split("\n").findAll{ it != ''}.collect{ it.trim() }
        return res
    }

    def getNested( String grp){
        //def res = [:]
        def findGrps = this.inventoryStruct.findAll{ it.value && it.value["children"] && it.value["children"].contains(grp) }.collect{ k, v -> k}
        println "findGrps: ${findGrps}"
        def res = [:]
        findGrps.each{ k ->
            if (res[k]){
                res[k]["children"].add(grp)
                res[k]["children"].unique{ a, b -> a <=> b }
            }else{
                 res << [(k): ["children": [grp]]]
            }
            def bufNext = getNested(k)
            
            bufNext.each{ n, n_data ->
                if (res[n]){
                    res[n]["children"] += n_data["children"]
                    res[n]["children"] = res[n]["children"].unique{ a, b -> a <=> b }
                }else{
                    res << [(n): ["children": n_data["children"]]]
                }
            }
            
        }
        println "findGrps res: ${res}"
        return res
    }
        

    def filterInventory(ArrayList hosts){
        def res = [:]
        this.inventoryStruct.findAll{ k,v -> k != "all" && v && v != [:] }.each{ k, content ->
            if (content["hosts"]){
                if (hosts){
                    def buf_hosts = content["hosts"].findAll{ hosts.contains(it) }
                    if (buf_hosts.size()>0){
                        res<<[(k): ["hosts": buf_hosts]]
                    }
                }else{
                    res<<[(k): ["hosts": content["hosts"]]]
                } 
            }
        }

        println "HostGroups: ${res}"

        res.collect{ k, v -> k}.each{ g ->
            def bufNested = getNested(g)
            println "bufNested: ${bufNested}"
            bufNested.each{	k, v ->
                println "bufNested: ${res}"
                if (res[k]){
                    println "res[k]: ${res[k]}"
                    res[k]["children"] += v["children"]
                    res[k]["children"] = res[k]["children"].unique{ a, b -> a <=> b }
                }else{
                    res<<[(k): ["children": v["children"]]]
                }
            }
        }
        println "res: ${res}"
        def rootGroups = this.inventoryStruct["all"]["children"].findAll{ g -> ! this.inventoryStruct.findAll{ it.value == [:]}.keySet().contains(g)}
        def resGroups = res.clone().keySet()
        println "rootGroups: ${rootGroups}"
        println "resgrp: ${resGroups}"
        //res << ["all": ["children": rootGroups.intersect(resGroups)]]
        return res
    }

}



class PlaybookParser implements Serializable{
  
    private final String workdir
    private final String rootPlaybookfilename
    private playbookStruct = [:]
  
    PlaybookParser(workdir, rootPlaybookfilename) {
        this.workdir = workdir
        this.rootPlaybookfilename = rootPlaybookfilename
    }
  
    def parseYaml(filepath){
        Yaml parser = new Yaml()
        def res = parser.load((filepath as File).text)
        return res
    }
  
    def generatePlaybookStruct(filename){
        return [[("${filename}".toString()): parseYaml("${this.workdir}/${filename}")]]
    }
  
    def appendIncludes(playbooksList){
        def result = []
        def tags_include = []
        playbooksList.each{ playbook ->
            playbook.each{ key, content ->
                content.each{ section ->
                    if (section.containsKey("tags")){
                    if (section["tags"] instanceof String){
                        tags_include += [section["tags"]]
                    }else{
                        tags_include.add(section["tags"])
                    }
                    }
                    
                    if (section.containsKey("include")){
                    def buf_scr = this.generatePlaybookStruct(section['include'])
                    if (buf_scr){
                    def include = this.appendIncludes(buf_scr)
                        include.each{ i_items ->
                        if (i_items['tags'].size()>0){
                            i_items['tags'] += tags_include
                        }else{
                            i_items['tags']=tags_include
                        }
                        result = result + include
                        }
                        
                    }
                                    
                    }else if (section.containsKey("import_playbook")){
                    def buf_scr = this.generatePlaybookStruct(section['import_playbook'])
                    if (buf_scr){
                    def  include = this.appendIncludes(buf_scr)
                        include.each{ i_items ->
                        if (i_items['tags'].size()>0){
                            i_items['tags'] += tags_include
                        }else{
                            i_items['tags']=tags_include
                        }
                        result = result + include
                        }
                        
                    }
                    }else{
                    result.add(section << ["tags": tags_include])
                    }
                
                }
            }
    }

    return result

    }
  
    void init(){
        def playbooks = this.generatePlaybookStruct(this.rootPlaybookfilename)
        this.playbookStruct = this.appendIncludes(playbooks)
    }

    def getRestartPatterns(){
        def all_patterns = []
        def restart_tags = ["stop", "start", "restart"]
        this.playbookStruct.each{ play ->
            def check = false
            if(play.tags.find{ pt -> restart_tags.contains(pt)}){
                check = true
            } else if ( play.roles && play.roles.find{ pr -> pr.tags.find{restart_tags.contains(it)}}){
                check = true
            } else if ( play.tasks && play.tasks.find{ pta -> pta.tags.find{restart_tags.contains(pta)}}){
                check = true
            }

            if(check == true){
                all_patterns << play.hosts
            }
        }

        return all_patterns.unique { a, b -> a <=> b }
    }

    def tags(def condition){
        def all_tags = []
        def other_tags = []
        this.playbookStruct.each{ play ->
            if (play.roles && play.roles.size()>0){
                play.roles.each{ role ->
                    if(condition){
                        if ((condition instanceof ArrayList && role.tags.find{condition.contains(it)}) || (! condition instanceof ArrayList && role.tags.find{it == condition})){
                            all_tags += role.tags
                        }else{
                            other_tags += role.tags
                        }
                    }else{
                        all_tags += role.tags
                    }
                    
                }
            }
            
            if (play.tasks && play.tasks.size()>0){
                play.tasks.each{ task ->
                    if(condition){
                        if ((condition instanceof ArrayList && task.tags.find{condition.contains(it)}) || (! condition instanceof ArrayList && task.tags.find{it == condition})){
                            all_tags += task.tags
                        }else{
                            other_tags += task.tags
                        }
                    }else{
                        all_tags += task.tags
                    }
                }
            }
            if(condition){
                if ((condition instanceof ArrayList && play.tags.find{condition.contains(it)}) || (! condition instanceof ArrayList && play.tags.find{it == condition})){
                    all_tags += play.tags
                }else{
                    other_tags += play.tags
                }
            }else{
                 all_tags += play.tags
            }
            
        }
        if (condition){
            return all_tags.unique { a, b -> a <=> b } - other_tags
        }else{
            return all_tags.unique { a, b -> a <=> b }
        }
        
    }

  
}



inventoryWorkDir = cloneGitRepo(inventoryWorkDir, repo_url, zone, "tags")


InventoryParser inventory = new InventoryParser("${inventoryWorkDir}/${zone}")
def extra_vars = parseYaml("${inventoryWorkDir}/extra_vars.yml")
def play_repo_url = extra_vars.artifact.path
def play_branch = extra_vars.artifact.version
def workDirPlay = play_repo_url.replaceAll(/(https|http):\/+.*?\//,'').replace('.git','')

workDirPlay = cloneGitRepo(workDirPlay, play_repo_url, play_branch, "")

PlaybookParser pp = new PlaybookParser(workDirPlay, playbook_path)
pp.init()

def restart_tags = jsonDump(pp.tags(["stop","start","restart"]))
def restart_patterns = pp.getRestartPatterns().join(",")
def hosts = inventory.getInventoryHosts(restart_patterns)

def res_inventory = inventory.filterInventory(hosts)



return"""
    
    <link rel="stylesheet" href="https://unpkg.com/bootstrap/dist/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.min.css" type="text/css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:700,400%7CDosis:400" media="all" />

    <link rel="stylesheet" href="https://unpkg.com/@riophae/vue-treeselect/dist/vue-treeselect.css" type="text/css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.0/css/all.css" type="text/css" />


    <!-- include Vue 2.x -->
    <script src="https://cdn.jsdelivr.net/npm/vue@^2" type="text/javascript"></script>
    <script src="https://unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.min.js" type="text/javascript"></script>
    
    <!-- include vue-treeselect & its styles. you can change the version tag to better suit your need. -->
    <script src="https://unpkg.com/@riophae/vue-treeselect" type="text/javascript"></script>
    
    
    <style>
    table.parameters {
        border-collapse: separate;
    }
    </style>


    <div id="app">
       <!-- <b-card no-body class="w-75 p-3 mb-12"> -->
            <b-row no-gutters>
                <b-col md="8">
                    <b-form-group label-cols="4" label-cols-lg="2" label="Limits" label-for="input-default">

                        <treeselect :options="inventory_options" :multiple="true" :default-expand-level="30"
                            :show-count="true" placeholder="Выберите группу или хост" :max-height="500" v-model="limits_select">
                            <label slot="option-label"
                                slot-scope="{ node, shouldShowCount, count, labelClassName, countClassName }"
                                :class="labelClassName">
                                <span><i :class="getLimitIcon(node.isBranch)"></i> {{ node.label }}</span>
                            </label>
                        </treeselect>
                    </b-form-group>

                    <b-form-group label-cols="4" label-cols-lg="2" label="Tags" label-for="input-default">
                        <treeselect :options="generateTagsOptions()" :multiple="true" :default-expand-level="1"
                            placeholder="Выберите тэг" :max-height="500" v-model="output.tags.include">
                        </treeselect>
                    </b-form-group>

                    <b-form-group label-cols="4" label-cols-lg="2" label="Skip Tags" label-for="input-default">
                        <treeselect :options="generateTagsOptions(true)" :multiple="true" :default-expand-level="1"
                            :show-count="true" placeholder="Выберите тэг, который хотите исключить" :max-height="500" v-model="output.tags.exclude">
                        </treeselect>
                    </b-form-group>

                    <b-form-group label-cols="4" label-cols-lg="2" label="Verbose" label-for="input-default">
                        <treeselect :options="verbose_options" :multiple="false" :default-expand-level="1"
                            :show-count="true" :clearable="false" :backspace-removes="false" placeholder="Выберите уровень логирования" v-model="output.verbose">
                        </treeselect>
                    </b-form-group>
                </b-col>
            </b-row>
         <!--  </b-card>  -->


        <input name="value" :value='result_data' id="result_data" hidden="true">
    </div>

    <script language="JavaScript" type="text/javascript">
        Q(document).ready(function () {

            new Vue({
                el: '#app',
                components: {
                    Treeselect: VueTreeselect.Treeselect
                },
                data: {
                    output: {
                        tags: {
                            include: [],
                            exclude: [],
                        },
                        extra_tags: null,
                        verbose: "v",
                        zone: "${zone}",
                        jobName: "${jobName}",
                        fullname: "${jobFullName}"
                    },
                    limits_select: [],
                    extra_tags: "",
                    verbose_options: [
                        {id:"" , label: "None"},
                        {id:"v" , label: "Low"},
                        {id:"vv" , label: "Medium"},
                        {id:"vvv" , label: "Hight"},
                        {id:"vvvv" , label: "Highest"},
                    ],
                    ansible_data:{
                        "inventory_data": {
                            "inventory_json": ${jsonDump(res_inventory)}
                        },
                        "playbook_data":{
                            "tags": ${restart_tags}
                        }
                    }
                },
                methods: {
                    getLimitIcon(isBranch) {
                        if (isBranch) {
                            return "fas fa-dice-d6"
                        } else {
                            return "fas fa-desktop"
                        }
                    },
                    getAllInventoryHosts(pattern) {
                        let res = []
                        const inventory_data = this.ansible_data.inventory_data.inventory_json
                        if (!pattern) {
                            for (var key in inventory_data) {
                                if ('hosts' in inventory_data[key]) {
                                    res.push.apply(res, inventory_data[key]['hosts'])
                                }
                            }
                        } else {
                            if (pattern in inventory_data) {
                                if ('hosts' in inventory_data[pattern]) {
                                    res.push.apply(res, inventory_data[pattern]['hosts'])
                                } else {
                                    if ('children' in inventory_data[pattern])
                                        inventory_data[pattern]['children'].forEach(c => {
                                            res.push.apply(res, this.getAllInventoryHosts(c))
                                        })
                                }
                            } else {
                                for (var key in inventory_data) {
                                    if ('hosts' in inventory_data[key]) {
                                        if (inventory_data[key]['hosts'].includes(pattern)) {
                                            res.push(pattern)
                                        }
                                    }
                                }
                            }
                        }
                        //console.log("res", res)

                        return res.filter((item, index) => res.indexOf(item) === index);
                    },
                    generateInventoryTree(key) {
                        const current = this.ansible_data.inventory_data.inventory_json[key]
                        let res = {
                            id: key,
                            label: key
                        }
                        if ('hosts' in current) {
                            res['children'] = []
                            current['hosts'].forEach(k => {
                                res['children'].push({
                                    id: k,
                                    label: k
                                })
                            });
                        } else {
                            if ('children' in current) {
                                res['children'] = []
                                current['children'].forEach(k => {
                                    res['children'].push(this.generateInventoryTree(k))
                                });
                            }
                        }
                        return res
                    },
                    generateTagsOptions(isExclude) {
                        let res = []
                        let items = []
                        const tags_items = this.ansible_data.playbook_data.tags
                        if (isExclude) {
                            if (this.output.tags["include"].length > 0) {
                                items = tags_items.filter(x => !this.output.tags["include"].includes(x))
                            } else {
                                items = tags_items
                            }
                        } else {
                            if (this.output.tags["exclude"].length > 0) {
                                items = tags_items.filter(x => !this.output.tags["exclude"].includes(x))
                            } else {
                                items = tags_items
                            }
                        }
                        items.forEach(item => {
                            //console.log(item)
                            res.push({
                                id: item,
                                label: item
                            })
                        });
                        return res
                    },
                    setExcludeTags(){
                        const tags_items = this.ansible_data.playbook_data.tags
                        console.log("tags_items",tags_items)
                        let res = []
                        if (tags_items && tags_items.includes('db')){
                            res.push('db')
                        }
                        if (tags_items && tags_items.includes('database')){
                            res.push('database')
                        }
                        
                        this.output.tags.exclude.push.apply(this.output.tags.exclude, res)
                    }

                },
                watch: {
                    ansible_data: {
                        handler(){
                            this.setExcludeTags()
                        },
                        deep: true
                    }
                },
                computed: {
                    // Все выбранные хосты
                    totalitems() {
                        return this.getAllInventoryHosts().length
                    },
                    // Лимиты
                    limits() {
                        const data = this.limits_select
                        if (data.length > 0) {
                            return data.join(',')
                        } else {
                            return 'all'
                        }

                    },
                    inventory_options() {
                        let res = []
                        this.ansible_data.inventory_data.inventory_json['all']['children'].forEach(
                            item => {
                                //console.log(item)
                                res.push(this.generateInventoryTree(item))
                            });
                        console.log('inventory_options', res)
                        return res
                    },
                    result_data() {
                        let res = this.output
                        res["limits"] = this.limits
                        return JSON.stringify(res)
                    },
                },
                mounted: function () {
                    this.setExcludeTags()
                }
            });
        });
    </script>
"""