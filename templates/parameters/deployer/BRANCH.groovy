def jsonParse(json) {
        new groovy.json.JsonSlurperClassic().parseText(json)
    }

def jobsJson = jsonParse(JOBS);
def gettags = ("git ls-remote -h ${jobsJson.GIT_HTTP_URL}${jobsJson.INVENTORY_PROJECT}").execute()
return gettags.text.readLines().collect { 
  it.split()[1].replaceAll('refs/heads/', '').replaceAll('refs/tags/', '').replaceAll("\\^\\{\\}", '')
}.sort()