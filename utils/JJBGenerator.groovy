package utils

/**
 * Класс содержит основные конфигурации для джобов, а также спецификации и методы по их настройке
 */
class JJBGenerator {

    static void generateJobConfig(job, data ) {
        job.with {
            parameters {
                stringParam ('TICKET_NUMBER', '', 'Номер тикета')
                stringParam ('AGENT_LABEL', 'master', 'Агент Jenkins')

                wReadonlyStringParameterDefinition {
                    name('ZONE')
                    defaultValue(data.zone)
                    description('Текущая зона деплоя')
                }

                /*
                wReadonlyStringParameterDefinition {
                    name('INVENTORY_PROJECT')
                    defaultValue(data.inventory_repo)
                    description('Проект с inventory репозиторием')
                }
                */

                stringParam ('BRANCH', 'master', 'Ветка репозитория с inventory')

                wReadonlyStringParameterDefinition {
                    name('PLAYBOOK')
                    defaultValue(data.playbook)
                    description('Плэйбук для запуска')
                }

                wReadonlyStringParameterDefinition {
                    name('AUTODEPLOY_SH')
                    defaultValue(data.useSh.toString())
                    description('Используется ли auto-deploy.sh')
                }

                stringParam ('VERBOSE', 'v', 'Уровень логирования')
                stringParam ('LIMITS', '', 'Лимиты инсталлятора')
                stringParam ('TAGS', 'all', 'Основные операции над приложением')
                stringParam ('EXTRA_OPTIONS', '', 'Дополнительные режимы ansible')

                nonStoredPasswordParam('PASSWORD', 'Пароль текущего пользователя')
                nonStoredPasswordParam('SECRET_EXTRA_VARS', 'Секретные переменные для инсталлятора')
                }


            definition {
                cps {
                    script(data.script)
                    sandbox()
                }
            }

            properties {
                buildDiscarder {
                    strategy {
                        logRotator {
                            daysToKeepStr("60")
                            numToKeepStr("20")
                            artifactDaysToKeepStr("")
                            artifactNumToKeepStr("")
                        }
                    }
                }
            }
        }
    }
}
